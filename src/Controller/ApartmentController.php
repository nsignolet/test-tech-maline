<?php

namespace App\Controller;

use App\Entity\Apartment;
use App\Form\ApartmentType;
use App\Repository\ApartmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="apartment_")
 */
class ApartmentController extends AbstractController
{
    /**
     * @Route("", name="index", methods={"GET"})
     */
    public function index(ApartmentRepository $apartmentRepository): Response
    {
        return $this->render('apartment/index.html.twig', [
            'apartments' => $apartmentRepository->findAll(),
        ]);
    }

    /**
     * @Route("new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $apartment = new Apartment();
        $form = $this->createForm(ApartmentType::class, $apartment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($apartment);
            $entityManager->flush();

            return $this->redirectToRoute('apartment_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('apartment/new.html.twig', [
            'apartment' => $apartment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("{id}", name="show", methods={"GET"})
     */
    public function show(Apartment $apartment): Response
    {
        return $this->render('apartment/show.html.twig', [
            'apartment' => $apartment,
        ]);
    }

    /**
     * @Route("{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Apartment $apartment): Response
    {
        $form = $this->createForm(ApartmentType::class, $apartment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('apartment_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('apartment/edit.html.twig', [
            'apartment' => $apartment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("{id}", name="delete", methods={"POST"})
     */
    public function delete(Request $request, Apartment $apartment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$apartment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($apartment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('apartment_index', [], Response::HTTP_SEE_OTHER);
    }
}
