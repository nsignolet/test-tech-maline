<?php

namespace App\Tests\Controller;

use App\Entity\Apartment;
use App\Repository\ApartmentRepository;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApartmentControllerTest extends WebTestCase
{
    use ReloadDatabaseTrait;

    /** @var KernelBrowser */
    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testApartmentsList(): void
    {
        $this->client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Apartments');
    }

    public function testNewApartment(): void
    {
        $this->client->request('GET', '/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Apartment');
    }

    public function testNewApartmentPostSuccess(): void
    {
        $this->client->request('GET', '/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Apartment');

        $this->client->submitForm(
            'Save',
            [
                "apartment[address]" => "1 Boulevard Malesherbes 75000 Paris",
                "apartment[floor]" => 2,
                "apartment[rooms]" => 1,
                "apartment[elevator]" => false
            ]
        );

        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertEquals('/', $this->client->getRequest()->getRequestUri());
        $this->assertSelectorTextContains('table', '1 Boulevard Malesherbes 75000 Paris');
    }

    public function testNewApartmentPostFailed(): void
    {
        $this->client->request('GET', '/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Apartment');

        $this->client->submitForm(
            'Save',
            [
                "apartment[address]" => "1 rue de la Paix 75000 Paris",
                "apartment[floor]" => 2,
                "apartment[rooms]" => 1,
                "apartment[elevator]" => false
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertEquals('/new', $this->client->getRequest()->getRequestUri());
        $this->assertSelectorTextContains('.form-error-message', 'This value is already used.');
    }

    public function testShowApartment(): void
    {
        $apartmentRepository = static::$container->get(ApartmentRepository::class);
        $apartment = $apartmentRepository->findOneByAddress('1 rue de la Paix 75000 Paris');
        $this->assertInstanceOf(Apartment::class, $apartment);

        $this->client->request('GET', '/' . $apartment->getId());

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Apartment');
        $this->assertSelectorTextContains('table', '1 rue de la Paix 75000 Paris');
    }

    public function testEditApartment(): void
    {
        $apartmentRepository = static::$container->get(ApartmentRepository::class);
        $apartment = $apartmentRepository->findOneByAddress('1 rue de la Paix 75000 Paris');
        $this->assertInstanceOf(Apartment::class, $apartment);

        $this->client->request('GET', '/' . $apartment->getId() . '/edit');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Edit Apartment');

        $this->client->submitForm(
            'Update',
            [
                "apartment[address]" => "2 rue de la Paix 75000 Paris",
                "apartment[floor]" => 2,
                "apartment[rooms]" => 1,
                "apartment[elevator]" => false
            ]
        );

        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertEquals('/', $this->client->getRequest()->getRequestUri());
        $this->assertSelectorTextContains('table', '2 rue de la Paix 75000 Paris');
    }

    public function testDeleteApartment(): void
    {
        $apartmentRepository = static::$container->get(ApartmentRepository::class);
        $apartment = $apartmentRepository->findOneByAddress('1 rue de la Paix 75000 Paris');
        $this->assertInstanceOf(Apartment::class, $apartment);

        $this->client->request('GET', '/' . $apartment->getId());

        $this->client->submitForm('Delete');

        $this->client->followRedirect();
        $this->assertResponseIsSuccessful();
        $this->assertEquals('/', $this->client->getRequest()->getRequestUri());

        $apartment = $apartmentRepository->findOneByAddress('1 rue de la Paix 75000 Paris');
        $this->assertNull($apartment);

    }
}
