<?php

namespace App\Tests\Repository;

use App\Entity\Apartment;
use Doctrine\ORM\EntityManager;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ApartmentRepositoryTest extends KernelTestCase
{
    use ReloadDatabaseTrait;

    /** @var EntityManager */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindAll()
    {
        $apartments = $this->entityManager->getRepository(Apartment::class)->findAll();
        $this->assertContainsOnlyInstancesOf(Apartment::class, $apartments);
    }

    public function testFindOneBy()
    {
        $apartment = $this->entityManager->getRepository(Apartment::class)->findOneBy(['address' => '1 rue de la Paix 75000 Paris']);

        $this->assertInstanceOf(Apartment::class, $apartment);
        $this->assertEquals('1 rue de la Paix 75000 Paris', $apartment->getAddress());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }



}
