<?php

namespace App\Tests\Entity;

use App\Entity\Apartment;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use PHPUnit\Framework\TestCase;

class ApartmentTest extends TestCase
{
    use ReloadDatabaseTrait;

    private $apartment;

    public function setUp(): void
    {
        $this->apartment = new Apartment();
    }

    public function testId(): void
    {
        $this->assertEquals(null, $this->apartment->getId());
    }

    public function testAddress(): void
    {
        $this->apartment->setAddress('1 Rue Lecourbe 75000 Paris');
        $this->assertEquals('1 Rue Lecourbe 75000 Paris', $this->apartment->getAddress());
    }

    public function testFloor(): void
    {
        $this->apartment->setFloor(2);
        $this->assertEquals(2, $this->apartment->getFloor());
    }

    public function testRooms():void
    {
        $this->apartment->setRooms(1);
        $this->assertEquals(1, $this->apartment->getRooms());
    }

    public function testElevator(): void
    {
        $this->apartment->setElevator(false);
        $this->assertEquals(false, $this->apartment->getElevator());
    }
}
