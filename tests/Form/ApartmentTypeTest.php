<?php

namespace App\Tests\Form;

use App\Entity\Apartment;
use App\Form\ApartmentType;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

class ApartmentTypeTest extends TypeTestCase
{
    use ReloadDatabaseTrait;

    protected function getExtensions()
    {
        return [new ValidatorExtension(Validation::createValidator())];
    }

    public function testApartmentFormView()
    {
        $data = new Apartment();
        $view = $this->factory->create(ApartmentType::class, $data)->createView();

        $this->assertArrayHasKey('address', $view->children);
        $this->assertArrayHasKey('floor', $view->children);
        $this->assertArrayHasKey('rooms', $view->children);
        $this->assertArrayHasKey('elevator', $view->children);
    }

    public function testSubmitAddressMinLength()
    {
        $data = [
            'address' => '1',
            'floor' => 0,
            'rooms' => 0,
            'elevator' => false
        ];

        $apartment = new Apartment();
        $form = $this->factory->create(ApartmentType::class, $apartment);

        $form->submit($data);

        $this->assertFalse($form->isValid());
    }

    public function testSubmitAddressMaxLength()
    {
        $data = [
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque efficitur imperdiet sem congue porta. Quisque tempor ultrices purus, ut volutpat dui tempor id. Pellentesque non tincidunt odio. Fusce laoreet, urna quis bibendum ornare, sapien mauris tempor mi, rutrum feugiat justo orci at biam.',
            'floor' => 0,
            'rooms' => 0,
            'elevator' => false
        ];

        $apartment = new Apartment();
        $form = $this->factory->create(ApartmentType::class, $apartment);

        $form->submit($data);

        $this->assertFalse($form->isValid());
    }

    public function testSubmitAddressBlank()
    {
        $data = [
            'address' => '',
            'floor' => 0,
            'rooms' => 0,
            'elevator' => false
        ];

        $apartment = new Apartment();
        $form = $this->factory->create(ApartmentType::class, $apartment);

        $form->submit($data);

        $this->assertFalse($form->isValid());
    }

    public function testSubmitFloorBlank()
    {
        $data = [
            'address' => '1 Boulevard de Belleville 75000 Paris',
            'floor' => '',
            'rooms' => 0,
            'elevator' => false
        ];

        $apartment = new Apartment();
        $form = $this->factory->create(ApartmentType::class, $apartment);

        $form->submit($data);

        $this->assertFalse($form->isValid());
    }

    public function testSubmitRoomsBlank()
    {
        $data = [
            'address' => '1 Rue Lecourbe 75000 Paris',
            'floor' => 0,
            'rooms' => '',
            'elevator' => false
        ];

        $apartment = new Apartment();
        $form = $this->factory->create(ApartmentType::class, $apartment);

        $form->submit($data);

        $this->assertFalse($form->isValid());
    }

    public function testSubmitValidData()
    {
        $data = [
            'address' => '1 Rue de Paradis 75000 Paris',
            'floor' => 1,
            'rooms' => 3,
            'elevator' => true
        ];

        $apartment = new Apartment();
        $form = $this->factory->create(ApartmentType::class, $apartment);

        $form->submit($data);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($apartment, $form->getData());
        $this->assertTrue($form->isSubmitted() && $form->isValid());
    }
}
