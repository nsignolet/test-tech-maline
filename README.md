## Test Technique Maline

### Pré-requis

- serveur web (Wamp64, ...)
- mise à jour des vhosts (en fonction de votre environnement)
- mise à jour des hosts (exemple ``127.0.0.1 testTech``)

### Installation

Veuillez finir d'installer le projet :
``$ composer install``

## Démarrage

Démarrer le serveur
``php -S localhost:8000 -t public
``

Rendez-vous sur la page d'accueil. Vous devriez voir apparaître : 
"Welcome to Symfony 4.4.32"



## A réaliser 

Développez une mini application qui permettra d'afficher et de modifier des appartements.

La page d'accueil actuelle "Welcome to Symfony 4.4.32" sera remplacée par la liste des appartements.

La liste des appartements permettra d'afficher les appartements et de les modifier. En effet, la sélection d'un appartement affichera un formulaire pour le mettre à jour.
Le formulaire peut-être affiché sur la même page ou sur une autre.

Un appartement est défini par : une adresse, un étage, un nombre de pièce, la présence d'un ascenceur.
La contrainte est que deux appartements ne peuvent pas avoir la même adresse.
Nous allons partir du principe qu'une adresse est stockée dans un unique champ.

Des tests unitaires devront stabiliser l'application.

N'oubliez pas de compléter le README.md (lancement des tests ect).

### Retour : 

Vous disposerez de 7 jours dès réception du test pour le renvoyer. Vous pouvez le renvoyer avant.
Une fois le test terminé, merci de l'envoyer à ``narnodo@maline-immobilier.fr`` et de préciser : 
- méthode pour récupérer le test (de préférence sur git sinon weTransfert)
- temps total passé sur le test
- reste à faire
- pistes d'amélioration dans le travail réalisé

#### Autre 

Je reste à votre disposition si nécessaire, n'hésitez surtout pas à me contacter si besoin : ``narnodo@maline-immobilier.fr``

Pour rappel, ce test évaluera principalement vos démarches et vos bonnes pratiques.

Le délais de 7 jours doit vous permettre de vous organiser, pas de travailler 10h sur le test ;)

### Utilisation 

* Se positionner dans le répertoire souhaité et récupérer le projet à l'aide de la commande
```
$ git clone https://gitlab.com/nsignolet/test-tech-maline.git
```
* Dupliquer le fichier .env vers .env.local et modificer ce nouveau fichier avec vos informations de base de données
* Pour installer l'ensemble des dépendances nécessaires au fonctionnement de l'application, éxécutez la commande suivante
```
$ composer install
```
* Exécutez les commandes suivantes pour installer la base de données
```
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:update --force
```
* Pour intégrer les données de démo dans la base données, éxécutez la commande suivante
```
$ php bin/console hautelook:fixtures:load
```
* Vous pouvez maintenant démarrer le server Symfony avec
```
$ php -S localhost:8000 -t public
```

### Lancement des tests

Créez une base de données de test (paramètres modifiables dans le fichier .env.test)  :
```
$ php bin/console doctrine:schema:create --env=test
```

Veuillez exécuter la commande suivante depuis la racine du projet pour lancer les tests et mettre à jour le code coverage  :
```
$ ./vendor/bin/phpunit --coverage-html test-coverage/
```